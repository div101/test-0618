import os
import pytest
from flask import url_for
from flask_migrate import upgrade

from src.app import create_app
from src.extensions import db as _db
from src.models import User, City, Region


@pytest.fixture(scope="session")
def app():
    app = create_app(testing=True)

    ctx = app.app_context()
    ctx.push()

    yield app

    ctx.pop()


@pytest.fixture(scope="session")
def db(app):
    _db.app = app

    # _db.create_all()
    upgrade()

    yield _db

    _db.session.close()
    _db.drop_all()
    os.unlink(_db.engine.url.database)


@pytest.fixture
def session(db):
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)
    db.session = session

    yield session

    transaction.rollback()
    connection.close()
    session.remove()


@pytest.fixture
def client(app):
    """A test client for the app."""
    return app.test_client()


@pytest.fixture
def user(session):
    user = User(
        name='user',
        login='user',
        password='user'
    )

    session.add(user)
    session.commit()
    return user


class AuthActions(object):
    def __init__(self, client, user):
        self._client = client
        self.user = user
        self.access_token = None
        self.refresh_token = None

    def login(self):
        url = url_for('auth.login')

        r = self._client.post(
            url, json={'login': self.user.login, 'password': self.user.login}
        )
        self.access_token = r.json['access_token']
        self.refresh_token = r.json['refresh_token']

    @property
    def headers(self):
        return {
            'authorization': 'Bearer ' + self.access_token
        }


@pytest.fixture
def auth(client, user):
    auth = AuthActions(client, user)
    auth.login()
    return auth


@pytest.fixture
def region(session):
    region = Region.query.get(1)
    return region


@pytest.fixture
def regions(session):
    r1 = Region.query.get(1)
    r2 = Region(name='r2', parent=r1)
    r3 = Region(name='r3', parent=r2)
    r4 = Region(name='r4', parent=r2)
    r5 = Region(name='r5', parent=r3)

    r1.descendants.extend([r3, r4, r5])
    r2.descendants.extend([r5])
    regions = [r1, r2, r3, r4, r5]

    session.add_all(regions)
    session.commit()
    return regions


@pytest.fixture
def cities(session, regions):
    cities = [City(name='city_' + r.name, region_id=r.id)
              for r in regions]

    session.add_all(cities)
    session.commit()
    return cities


@pytest.fixture
def city(session, region):
    city = City(
        name='city',
        region_id=region.id
    )

    session.add(city)
    session.commit()
    return city
