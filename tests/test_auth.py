import pytest
from flask import url_for


@pytest.mark.parametrize('payload, msg', (
    ({'login': 'aa', 'password': 'aa'}, b'Bad credentials'),
    ({'login': 'user', 'password': 'aa'}, b'Bad credentials'),
    ({'password': 'aa'}, b'Missing username or password'),
    ({'login': 'aa'}, b'Missing username or password'),
))
def test_wrong_login(client, user, payload, msg):
    url = url_for('auth.login')
    r = client.post(url, json=payload)
    assert r.status_code == 400, r.get_data(as_text=True)
    assert msg in r.data


def test_login(client, user):
    url = url_for('auth.login')
    payload = {
        'login': user.login,
        'password': user.login,
    }

    r = client.post(url, json=payload)
    assert r.status_code == 200, r.get_data(as_text=True)
    assert set(r.json.keys()) == {'access_token', 'refresh_token'}


def test_refresh(client, auth):
    url = url_for('auth.refresh')
    r = client.post(url, headers={'authorization': 'Bearer ' + auth.refresh_token})
    assert r.status_code == 200, r.get_data(as_text=True)
    assert set(r.json.keys()) == {'access_token'}
