from itertools import chain

import pytest
from flask import url_for

from src.models import City
from src.utils import slice_dict


def test_get_cities(client, auth, city):
    city_keys = {'id', 'name', 'region_id'}
    url = url_for('regions.citylistresource')
    detail_url = url_for('regions.cityresource', obj_id=city.id)

    r = client.get(url)
    assert r.status_code == 401

    r = client.get(url, headers=auth.headers)
    assert r.status_code == 200
    assert r.is_json
    assert set(r.json.keys()) == {'total', 'pages', 'results'}
    assert r.json['total'] == City.query.count()
    assert set(r.json['results'][0].keys()) == city_keys

    r = client.get(detail_url, headers=auth.headers)
    assert r.status_code == 200
    assert set(r.json) == city_keys


def test_post_city(client, auth, region):
    url = url_for('regions.citylistresource')
    payload = {
        'name': 'city',
        'region_id': region.id
    }

    r = client.post(url, json=payload, headers=auth.headers)
    assert r.status_code == 201
    assert slice_dict(r.json, payload.keys()) == payload


def test_patch_city(client, auth, city):
    url = url_for('regions.cityresource', obj_id=city.id)

    payload = {
        'name': 'new_city',
    }

    r = client.patch(url, json=payload, headers=auth.headers)
    assert r.status_code == 200
    assert slice_dict(r.json, payload.keys()) == payload


@pytest.mark.parametrize('payload', (
    {'bad_field': 'city', 'region_id': 1},
    {'name': 11111, 'region_id': 1},
    {'name': [], 'region_id': 1},
    {'name': 'city', 'region_id': 999},
))
def test_post_bad_payload_city(client, auth, payload, region):
    url = url_for('regions.citylistresource')

    r = client.post(url, json=payload, headers=auth.headers)
    assert r.status_code == 422, r.get_data(as_text=True)


def test_delete_city(client, auth, city):
    url = url_for('regions.cityresource', obj_id=city.id)

    r = client.delete(url, headers=auth.headers)
    assert r.status_code == 204
    assert City.query.filter_by(id=city.id).first() is None


def test_filter_by_region(client, auth, cities):
    region = cities[1].region
    cities = set(chain.from_iterable(
        [r.cities for r in region.descendants] +
        [r.cities for r in region.children] +
        [region.cities]))

    url = url_for('regions.citylistresource', region_id=region.id)

    r = client.get(url, headers=auth.headers)
    assert r.status_code == 200
    assert r.is_json
    assert r.json['total'] == len(cities)
