from flask import url_for

from src.models import Region
from src.utils import slice_dict


def test_relationsips(session):
    r1 = Region(name='r1')
    r2 = Region(name='r2', parent=r1)
    r3 = Region(name='r3', parent=r2)
    r4 = Region(name='r4', parent=r2)
    r5 = Region(name='r5', parent=r3)
    r1.descendants.extend([r3, r4, r5])
    r2.descendants.extend([r5])
    session.add_all([r1, r2, r3, r4, r5])
    session.commit()

    assert set(r1.descendants) == {r3, r4, r5}
    assert set(r2.children) == {r3, r4}
    assert set(r2.descendants) == {r5}
    assert set(r5.ancestors) == {r1, r2}


def test_get_regions(client, auth, regions):
    region_keys = {'id', 'name', 'parent_id'}
    with client.application.app_context():
        url = url_for('regions.regionlistresource')
        detail_url = url_for('regions.regionresource', obj_id=regions[0].id)

    r = client.get(url, headers=auth.headers)
    assert r.status_code == 200
    assert r.is_json
    assert r.json['total'] == Region.query.count()
    assert set(r.json['results'][0].keys()) == region_keys

    r = client.get(detail_url, headers=auth.headers)
    assert r.status_code == 200
    assert set(r.json) == region_keys


def test_post_region(client, auth, region):
    url = url_for('regions.regionlistresource')
    payload = {
        'name': 'region',
        'parent_id': region.id
    }

    r = client.post(url, json=payload, headers=auth.headers)
    assert r.status_code == 201
    assert slice_dict(r.json, payload.keys()) == payload


def test_patch_region(client, auth, region):
    url = url_for('regions.regionresource', obj_id=region.id)

    payload = {
        'name': 'new_region',
    }

    r = client.patch(url, json=payload, headers=auth.headers)
    assert r.status_code == 200
    assert slice_dict(r.json, payload.keys()) == payload


def test_delete_city(client, auth, regions):
    url = url_for('regions.regionresource', obj_id=regions[0].id)
    old_count = Region.query.count()
    r = client.delete(url, headers=auth.headers)
    assert r.status_code == 204
    assert Region.query.filter_by(id=regions[0].id).first() is None
    assert old_count - Region.query.count() > 1


def test_get_regions_as_tree(client, auth, regions):
    url = url_for('regions.regionlistresource', as_tree='')

    r = client.get(url, headers=auth.headers)
    assert r.status_code == 200
    assert 'children' in r.json
    assert (set(i['id'] for i in r.json['children']) ==
            set(i.id for i in regions[0].children))
