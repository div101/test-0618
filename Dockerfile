FROM python:3.6-alpine

# disable pip cache
ENV PIP_NO_CACHE_DIR=false

RUN pip install pipenv

WORKDIR /app

COPY Pipfile* /app/

RUN pipenv install --system

COPY /src ./src

COPY /migrations ./migrations

ENV FLASK_ENV=development \
    FLASK_APP="src.app:create_app"

EXPOSE 5000

CMD flask db upgrade && flask run -h 0.0.0.0
