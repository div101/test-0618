from flask import Flask

from . import auth
from . import regions
from .extensions import db, jwt, migrate


def create_app(testing=False):
    app = Flask('test')

    configure_app(app, testing)
    configure_extensions(app)
    register_blueprints(app)

    return app


def configure_app(app, testing=False):
    if testing is True:
        app.config.from_object('src.config.TestingConfig')
    else:
        app.config.from_object('src.config.DevConfig')

    if app.config.get('SQLALCHEMY_DATABASE_URI', '').startswith('sqlite'):
        # enable foreign key constraints checks
        # they are turned off by default
        set_sqlite_pargma_listener()


def configure_extensions(app):
    db.init_app(app)
    jwt.init_app(app)
    migrate.init_app(app, db)


def register_blueprints(app):
    app.register_blueprint(auth.views.blueprint, url_prefix='/auth')
    app.register_blueprint(regions.views.blueprint, url_prefix='/api')


def set_sqlite_pargma_listener():
    from sqlalchemy.engine import Engine
    from sqlalchemy import event

    @event.listens_for(Engine, "connect")
    def set_sqlite_pragma(dbapi_connection, connection_record):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON")
        cursor.close()
