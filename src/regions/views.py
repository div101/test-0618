from flask import Blueprint
from flask_restful import Api

from .resources import CityListResource, CityResource, RegionResource, RegionListResource

blueprint = Blueprint('regions', __name__)
api = Api(blueprint)


api.add_resource(CityResource, '/cities/<int:obj_id>')
api.add_resource(CityListResource, '/cities')

api.add_resource(RegionResource, '/regions/<int:obj_id>')
api.add_resource(RegionListResource, '/regions')
