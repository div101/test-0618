from itertools import groupby

from flask import request
from sqlalchemy.orm import aliased

from src.base import BaseResource, BaseListResource
from src.models import City, Region
from src.regions.schemas import CitySchema, RegionSchema


class CityResource(BaseResource):
    query = lambda *_: City.query
    schema = CitySchema


class CityListResource(BaseListResource):
    schema = CitySchema

    def query(self):
        region_id = request.args.get('region_id')
        query = City.query
        if region_id:
            # getting cities in selected region and its descendants
            ancestors = aliased(Region)
            query = (
                query.
                join(City.region).
                join(ancestors, Region.ancestors, isouter=True).
                filter(
                    (Region.id == region_id) |
                    (Region.parent_id == region_id) |
                    (ancestors.id == region_id)
                ).distinct()
            )
        return query


class RegionResource(BaseResource):
    query = lambda *_: Region.query
    schema = RegionSchema


class RegionListResource(BaseListResource):
    query = lambda *_: Region.query
    schema = RegionSchema

    def get(self):
        as_tree = 'as_tree' in request.args

        if as_tree:
            return self.as_tree()
        return super().get()

    def as_tree(self):
        data = self.schema(many=True).dump(self.query).data
        by_id = {i['id']: i for i in data}

        root = None
        key = lambda x: (x['parent_id'] or -1)
        data = sorted(data, key=key)
        for pid, children in groupby(data, key=key):
            if pid is -1:
                root = next(children)
                continue
            parent = by_id[pid]
            parent['children'] = list(children)

        return root, 200
