from marshmallow import validates, ValidationError

from src.extensions import ma, db
from src.models import City, Region
from src.utils import get_relationships


def check_region_existance(region_id):
    region_exists = Region.query.filter_by(id=region_id).exists()
    if not db.session.query(region_exists).scalar():
        raise ValidationError('Such region does not exist.')


class CitySchema(ma.ModelSchema):

    @validates('region_id')
    def validate_region(self, value):
        check_region_existance(value)

    class Meta:
        model = City
        include_fk = True
        exclude = get_relationships(City)
        sqla_session = db.session


class RegionSchema(ma.ModelSchema):

    @validates('parent_id')
    def validate_region(self, value):
        check_region_existance(value)

    class Meta:
        model = Region
        include_fk = True
        exclude = get_relationships(Region)
        sqla_session = db.session
