from flask import request, jsonify, Blueprint
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity
)

from src.models import User
from src.extensions import pwd_context, jwt
from src.utils import json_view

blueprint = Blueprint('auth', __name__, url_prefix='/auth')


@blueprint.route('/login', methods=['POST'])
@json_view
def login():
    """Authenticate user and return token
    """
    login = request.json.get('login', None)
    password = request.json.get('password', None)
    if not login or not password:
        return {'msg': 'Missing username or password'}, 400

    user = User.query.filter_by(login=login).first()
    if user is None or not pwd_context.verify(password, user.password):
        return {'msg': 'Bad credentials'}, 400

    ret = {
        'access_token': create_access_token(identity=user.id),
        'refresh_token': create_refresh_token(identity=user.id)
    }
    return ret, 200


@blueprint.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
@json_view
def refresh():
    current_user = get_jwt_identity()
    ret = {
        'access_token': create_access_token(identity=current_user)
    }
    return ret, 200


@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    return User.query.get(identity)
