import functools

from flask import request, jsonify


def slice_dict(d, keys):
    return {k: d[k] for k in keys}


def get_relationships(model):
    return tuple(
        prop.key
        for prop in model.__mapper__.iterate_properties
        if hasattr(prop, 'direction')
    )


def json_view(func):

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if request.data and not request.is_json:
            return jsonify({'msg': 'Missing JSON in request'}), 400
        result = func(*args, **kwargs)
        return (jsonify(result[0]), *result[1:])

    return wrapper
