import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    BASEDIR = BASEDIR
    MAX_CONTENT_LENGTH = 8 * (1024 ** 2)
    DEBUG = False
    SECRET_KEY = 'secret'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'app.sqlitedb')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True


class ProductionConfig(Config):
    JSONIFY_PRETTYPRINT_REGULAR = False


class DevConfig(Config):
    JSONIFY_PRETTYPRINT_REGULAR = True
    SQLALCHEMY_ECHO = True
    DEBUG = True
    ASSETS_DEBUG = True


class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/app_testing.sqlitedb'
    SERVER_NAME = 'localhost.local'
