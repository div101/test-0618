from src.extensions import db


region_link = db.Table(
    'region_link',
    db.Column('ancestor_id', db.Integer,
              db.ForeignKey('region.id', ondelete='CASCADE'), primary_key=True),
    db.Column('descendant_id', db.Integer,
              db.ForeignKey('region.id', ondelete='CASCADE'), primary_key=True),
    db.UniqueConstraint('ancestor_id', 'descendant_id')
)


class Region(db.Model):
    __tablename__ = 'region'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('region.id', ondelete='CASCADE'),
                          nullable=True)

    parent = db.relationship(
        'Region', remote_side=[id],
        backref=db.backref('children', cascade="all,delete,delete-orphan"))
    descendants = db.relationship(
        'Region',
        secondary=region_link,
        primaryjoin=id == region_link.c.ancestor_id,
        secondaryjoin=id == region_link.c.descendant_id,
        backref=db.backref('ancestors')
    )

    def __repr__(self):
        return f'<Region id={self.id} name={self.name}>'
