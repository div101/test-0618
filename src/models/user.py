from src.extensions import db, pwd_context


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    login = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.password = pwd_context.hash(self.password)

    def __repr__(self):
        return f'<User id={self.id} login={self.login}>'
