from src.extensions import db


class City(db.Model):
    __tablename__ = 'city'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    region_id = db.Column(db.Integer, db.ForeignKey('region.id'), nullable=False)

    region = db.relationship('Region', backref=db.backref('cities'))

    def __repr__(self):
        return f'<City id={self.id} name={self.name}>'
