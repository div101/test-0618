from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource

from src.extensions import db


class BaseResource(Resource):
    query = None
    schema = None
    method_decorators = [jwt_required]

    def __init__(self, *args, **kwargs):
        assert self.query is not None
        assert self.schema is not None
        super().__init__(*args, **kwargs)
        self.query = self.query()

    def get(self, obj_id):
        obj = self.query.get_or_404(obj_id)
        return self.schema().dump(obj).data

    def patch(self, obj_id):
        schema = self.schema(partial=True)
        obj = self.query.get_or_404(obj_id)
        obj, errors = schema.load(request.json, instance=obj)
        if errors:
            return errors, 422

        return schema.dump(obj).data

    def delete(self, obj_id):
        obj = self.query.get_or_404(obj_id)
        db.session.delete(obj)
        db.session.commit()
        return '', 204


class BaseListResource(Resource):
    query = None
    schema = None
    method_decorators = [jwt_required]

    default_page_size = 10

    def __init__(self, *args, **kwargs):
        assert self.query is not None
        assert self.schema is not None
        super().__init__(*args, **kwargs)
        self.query = self.query()

    def get(self):
        page = request.args.get('page', 1)
        objects = self.query.paginate(page=page, per_page=self.default_page_size)
        return {
            'total': objects.total,
            'pages': objects.pages,
            'results': self.schema(many=True).dump(objects.items).data
        }

    def post(self):
        schema = self.schema()
        obj, errors = schema.load(request.json)
        if errors:
            return errors, 422

        db.session.add(obj)
        db.session.commit()
        return schema.dump(obj).data, 201
