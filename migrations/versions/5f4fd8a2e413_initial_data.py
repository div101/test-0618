"""initial_data

Revision ID: 5f4fd8a2e413
Revises: fb12b69acf64
Create Date: 2018-07-02 08:05:12.775636

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5f4fd8a2e413'
down_revision = 'fb12b69acf64'
branch_labels = None
depends_on = None


region = sa.table('region',
                  sa.Column('id', sa.Integer(), nullable=False),
                  sa.Column('name', sa.String(length=255), nullable=False),
                  sa.Column('parent_id', sa.Integer(), nullable=True),
                  )

user = sa.table('user',
                sa.Column('id', sa.Integer(), nullable=False),
                sa.Column('name', sa.String(length=255), nullable=False),
                sa.Column('login', sa.String(length=255), nullable=False),
                sa.Column('password', sa.String(length=255), nullable=False),
                )


def upgrade():
    op.bulk_insert(region, [
       {'id': 1, 'name': 'World', 'parent_id': None},
    ])
    op.bulk_insert(user, [
       {'id': 1, 'login': 'admin', 'name': 'admin',
        'password': '$pbkdf2-sha256$29000$U0opJUTIeU/JuRdCSAnhXA$'
                    'ptIUFbABlmCnJtd17nQQaeuA5OSRUUX/mKaN6xQbKdk'},
    ])


def downgrade():
    pass
